//
//  HomeController.swift
//  LanguageDemo
//
//  Created by Jinying Zhou 周金英 on 2018/11/9.
//  Copyright © 2018年 Jinying Zhou 周金英. All rights reserved.
//

import UIKit

class HomeController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.white

        let label = UILabel.init(frame: CGRect(x: 0, y: 100, width: view.frame.width, height: 50))
        label.text = CustomLocalizedString("你好")
        label.textAlignment = .center
        view.addSubview(label)

        let imageView = UIImageView(frame: CGRect(x: 0, y: 200, width: 100, height: 100))
        imageView.center.x = view.center.x
        imageView.image = UIImage(named: CustomLocalizedString("pic"))
        view.addSubview(imageView)
    }
}
