//
//  LanguageHandler.swift
//  LanguageDemo
//
//  Created by Jinying Zhou 周金英 on 2018/11/10.
//  Copyright © 2018年 Jinying Zhou 周金英. All rights reserved.
//

import UIKit

private let KUserLanguage: String = "UserLanguage"

class LanguageHandler: NSObject {
    // 单例
    static let shared = LanguageHandler()
    // 当前语言所在Bundle
    var currentLanguageBundle:Bundle?

    private override init() {
        super.init()
        initLanguages()
    }

    /// 初始化
    func initLanguages() {
        let userDefaults = UserDefaults.standard
        var userLanguage = getCurrentLanguage()
        // 未设置过用户语言，则选择手机默认系统语言
        if userLanguage == nil {
            guard let systemLanguageArray: [String] = (userDefaults.object(forKey: "AppleLanguages") as? Array<String>) else {
                return
            }
            let language = systemLanguageArray[0]
            userLanguage = managerSystemLanguageName(languageString: language)
        }
        changeLanaguage(language: userLanguage)
    }

    /// 切换用户语言
    ///
    /// - Parameter language: 语言名称
    func changeLanaguage(language: String?) {
        guard let currentLanguage = language else {
            print("语言为空")
            return
        }
        if getCurrentLanguage() == currentLanguage {
            print("切换语言一致，不做处理")
        }

        if let path = Bundle.main.path(forResource: currentLanguage, ofType: "lproj" ) {
            currentLanguageBundle = Bundle(path:path)
            saveCurrentLanguage(language: currentLanguage)
            // 替换bundle
            object_setClass(Bundle.main, CustomBundle.self)
        }
    }

    /// 获取用户当前使用的语言
    ///
    /// - Returns: 当前使用语言
    func getCurrentLanguage() -> String? {
        let userDefaults = UserDefaults.standard
        let currentLanguage = userDefaults.string(forKey: KUserLanguage)
        return currentLanguage
    }

    /// 存储当前语言
    func saveCurrentLanguage(language: String) {
        // 存储当前语言
        let userDefaults = UserDefaults.standard
        userDefaults.set(language, forKey: KUserLanguage)
        userDefaults.synchronize()
    }

    /// 处理语言名称 升级ios9之后，使得原本支持中英文的app出现闪退，中英文混乱的问题！原因是升级之后中英文目录名字改了。在真机上，中文资源目录名由zh-Hans---->zh-Hans-CN，英文资源目录名由en---->en-CN，ios9模拟器上面的中英文资源目录名和真机上面的不一样，分别是zh-Hans-US，en-US。
    ///
    /// - Parameter languageString: 语言名
    /// - Returns: 处理后语言名
    func managerSystemLanguageName(languageString: String) -> String{
        var language = languageString
        if language.hasPrefix("zh-Hans") {
            language = "zh-Hans"
        }else if language.hasPrefix("en") {
            language = "en"
        }
        return language
    }
}

func CustomLocalizedString(_ key:String) -> String {
    let bundle = LanguageHandler.shared.currentLanguageBundle
    if let str = bundle?.localizedString(forKey: key, value: nil, table: nil) {
        return str
    }
    return key
}

class CustomBundle: Bundle {
    override func localizedString(forKey key: String, value: String?, table tableName: String?) -> String {
        if let bundle = LanguageHandler.shared.currentLanguageBundle {
            return bundle.localizedString(forKey: key, value: value, table: tableName)
        } else {
            return super.localizedString(forKey: key, value: value, table: tableName)
        }
    }
}
