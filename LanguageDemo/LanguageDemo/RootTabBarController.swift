//
//  RootTabBarController.swift
//  LanguageDemo
//
//  Created by Jinying Zhou 周金英 on 2018/11/9.
//  Copyright © 2018年 Jinying Zhou 周金英. All rights reserved.
//

import UIKit

class RootTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

//        let handler = LanguageHandler()
//        handler.initUserLanguage()
        createViewControllers()
    }

    func createViewControllers() {

        let homeController = HomeController()
        let homeItem : UITabBarItem = UITabBarItem (title: "首页", image: UIImage(named: "home"), selectedImage: UIImage(named: "home"))
        homeController.tabBarItem = homeItem

        let secondController = SecondController()
        let secondItem : UITabBarItem = UITabBarItem (title: "视图", image: UIImage(named: "set"), selectedImage: UIImage(named: "favor"))
        secondController.tabBarItem = secondItem

        let setController = SetController()
        let setItem : UITabBarItem = UITabBarItem (title: "设置", image: UIImage(named: "set"), selectedImage: UIImage(named: "favor"))
        setController.tabBarItem = setItem

        let controllersArray = [homeController, secondController ,setController]
        self.viewControllers = controllersArray
    }
}
