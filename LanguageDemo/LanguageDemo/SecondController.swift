//
//  SecondController.swift
//  LanguageDemo
//
//  Created by Jinying Zhou 周金英 on 2018/11/10.
//  Copyright © 2018年 Jinying Zhou 周金英. All rights reserved.
//

import UIKit

class SecondController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        imageView.image = UIImage.init(named: CustomLocalizedString("pic"))
    }
}
