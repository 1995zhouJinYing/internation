//
//  SetController.swift
//  LanguageDemo
//
//  Created by Jinying Zhou 周金英 on 2018/11/9.
//  Copyright © 2018年 Jinying Zhou 周金英. All rights reserved.
//

import UIKit

class SetController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    var array: [[String]] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.white
        array = [["中文","zh-Hans"],["English","en"],["法语","fr"]]
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = array[indexPath.row][0]
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let language = array[indexPath.row][1]
        //切换语言
        LanguageHandler.shared.changeLanaguage(language: language)
        // 更新界面
        guard let window = UIApplication.shared.delegate?.window else {
            return
        }
        let root = RootTabBarController()
        window!.rootViewController = root
    }
}
